module Hangman.Input

||| User input can only be one letter at a time, so we reflect it in the type.
public export
data UserInput : List Char -> Type where
     Letter : (c : Char) -> UserInput [c]

||| Is not possible for an empty input to be valid.
validNull : UserInput [] -> Void
validNull (Letter _) impossible

||| Is not possible for multiple-char input to be valid.
validMultiple : UserInput (x :: (y :: xs)) -> Void
validMultiple (Letter _) impossible

||| Test the validity of a list of characters as user input for the game.
testValidity : (cs : List Char) -> Dec (UserInput cs)
testValidity [] = No validNull
testValidity (x :: []) = Yes (Letter x)
testValidity (x :: (y :: xs)) = No validMultiple

||| Test the validity of a string as a user input.
isValidString : (s : String) -> Dec (UserInput (unpack s))
isValidString s = testValidity (unpack s)


||| Prompt user for input and test if it is valid for the game, else prompt again.
export
readGuess : IO (x ** UserInput x)
readGuess = do
  putStr "Guess: "
  x <- getLine
  case isValidString x of
       Yes prf => pure (_ ** prf)
       No contra => do putStrLn "Sorry, your input was not valid."
                       readGuess
