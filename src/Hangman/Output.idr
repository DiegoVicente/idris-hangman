module Hangman.Output

import Hangman.State
import Data.Vect


||| Return the secret word only displaying the letters that have been correctly
||| guessed. The rest of the word is hidden with underscores.
export
conceil : WordState (S lives) (S letters) -> String
conceil (MkWordState word missing) = (pack . (map (hide missing)) . unpack) word

  where hide : (missing : Vect letters Char) -> (letter : Char) -> Char
        hide missing letter = if (toUpper letter) `elem` missing then '_' else letter

||| The hangman drawing
export
drawing : (lives : Nat) -> String
drawing n = case toIntNat n of
  0 => """
      _______
     |/      
     |      (_)
     |      \|/
     |       |
     |      / \
     |
    _|___
"""
  1 => """
      _______
     |/      
     |      (_)
     |      \|/
     |       |
     |        \
     |
    _|___
"""
  2 => """
      _______
     |/      
     |      (_)
     |      \|/
     |       |
     |      
     |
    _|___
"""
  3 => """
      _______
     |/      
     |      (_)
     |      \|
     |       |
     |      
     |
    _|___
"""
  4 => """
      _______
     |/      
     |      (_)
     |       |
     |       |
     |      
     |
    _|___
"""
  5 => """
      _______
     |/      
     |      (_)
     |
     |
     |
     |
    _|___
"""
  _ => """
      _______
     |/      
     |
     |
     |
     |
     |
    _|___
"""

||| The losing sprite for the game over dialog.
export
hangman : String
hangman = """
      _______
     |/      |
     |      (_)
     |      /|\
     |       |
     |      / \
     |
    _|___
"""
