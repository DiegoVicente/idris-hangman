module Hangman.State

import Data.Vect

||| The current state of the game.
||| @lives : The number of guesses that the player has not used yet.
||| @letters : The number of letters that the player still has to guess.
public export
data WordState : (lives : Nat) -> (letters : Nat) -> Type where
     MkWordState : (word : String) -> (missing : Vect letters Char) ->
                   WordState lives letters

%name WordState st, st', st''

||| Example game used for testing purposes
export
testGame : WordState 7 3
testGame = MkWordState "Test" ['T', 'E', 'S']


||| If a game is finished and whether the result is a win or a loss.
public export
data Finished : Type where
     ||| A game is a victory if no more letters have to be guessed.
     Won  : (game : WordState (S lives) 0)   -> Finished
     ||| A game is a loss if there are no more lives left.
     Lost : (game : WordState 0 (S letters)) -> Finished


||| Remove a value from a vector as long as it is contained in it.
||| @value: element to remove from the vector.
||| @xs: vector to update.
||| @prf: prove that value is contained inside vector.
remove : (value : a) -> (xs : Vect (S n) a) ->
         {auto prf : Elem value xs} -> 
         (Vect n a)
remove value (value :: ys) {prf = Here} = ys
remove {n = Z} value (y :: []) {prf = There later} = absurd later
remove {n = (S k)} value (y :: ys) {prf = There later} = 
  y :: remove value ys


||| Check if the letter is part of the secret word of the game.
export
processGuess : (letter : Char) ->
               WordState (S lives) (S letters) ->
               Either (WordState lives (S letters))
                      (WordState (S lives) letters)
processGuess letter (MkWordState word missing) = 
  case isElem (toUpper letter) missing of
       Yes prf => Right $ MkWordState word (remove (toUpper letter) missing)
       No contra => Left $ MkWordState word missing

