module Main

import Hangman.State
import Hangman.Input
import Hangman.Output

import Data.Vect


||| Top-level definition of game's main loop.
game : WordState (S lives) (S letters) -> IO Finished
game {lives} {letters} st = do
  putStrLn $ drawing lives
  putStrLn $ "\nWord is " ++ (conceil st)
  (_ ** Letter letter) <- readGuess
  case processGuess letter st of
       Left st' => do putStrLn "Wrong!"
                      case lives of
                           Z => pure (Lost st')
                           S k => game st'
       Right st' => do putStrLn "Correct!"
                       case letters of
                            Z => pure (Won st')
                            S k => game st'


||| Main method to launch the game.
main : IO ()
main = do 
  putStrLn "Welcome to the Idris Hangman game!\n"
  result <- game {lives=6} testGame
  case result of
    Lost (MkWordState word missing) => do
      putStrLn hangman
      putStrLn ("\nYou lose. The word was " ++ word)
    Won game =>
      putStrLn "\nCongratulations, you win!"
